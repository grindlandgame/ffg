﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class MenuStartGame : MonoBehaviour {
	public NetworkManager NetManager;
	public GameObject SetIPInputFiled;
	public GameObject SetPortInputFiled;
	public GameObject ModeDropdown;
	public GameObject PlayButton;
	public string IP;
	public int PORT;
	public Multiplayer.Modes Mode;
	public GameObject EventSystem;

	private Camera _CameraStartGame;
	private Canvas _MenuStartGame;
	private InputField _SetIPInputFiled;
	private InputField _SetPortInputFiled;
	private Dropdown _ModeDropdown;
	private Text _PlayButton;
	private string _LoadedMap;
	private bool _ShowPlayEnabled;

	private void ShowPlayMenu(bool show)
	{
		_MenuStartGame.enabled = show;
		_SetIPInputFiled.enabled = show;
		_SetPortInputFiled.enabled = show;
		_CameraStartGame.enabled = show;
		_ShowPlayEnabled = show;
		if (_LoadedMap == "") {
			_PlayButton.text = "Połącz";
		} else {
			_PlayButton.text = "Rozłącz";
		}
	}
	public void IPChanged(string ip)
	{
		IP = ip;
	}
	public void PortChanged(string port)
	{
		if (!System.Int32.TryParse (port,out this.PORT))
		{
			PORT = 0;
		}
	}
	public void ModeChanged(int M)
	{
		Mode = (Multiplayer.Modes)M;
		if (Mode == Multiplayer.Modes.Host) {
			_SetIPInputFiled.enabled = false;
		} else
		{
			_SetIPInputFiled.enabled = true;
		}
	}
	public void PlayGame()
	{
		if (_LoadedMap == "") {
			ShowPlayMenu (false);
			_LoadedMap = "Map";
			NetManager.onlineScene = _LoadedMap;
			SceneManager.LoadScene (_LoadedMap, LoadSceneMode.Additive);
			if (Mode == Multiplayer.Modes.Client) {
				NetManager.StartClient ();
			} else if (Mode == Multiplayer.Modes.Host) {
				NetManager.StartHost ();
			} else if (Mode == Multiplayer.Modes.Server) {
				NetManager.StartServer ();
			}
		} else {
			if (Mode == Multiplayer.Modes.Client) {
				NetManager.StopClient ();
			} else if (Mode == Multiplayer.Modes.Host) {
				NetManager.StopHost ();
			} else if (Mode == Multiplayer.Modes.Server) {
				NetManager.StopServer ();
			}
			Destroy (EventSystem);
			Destroy (NetManager.gameObject);
			Destroy (gameObject, 1f);
			SceneManager.LoadScene("PlayMenu",LoadSceneMode.Single);
			/*/
			SceneManager.UnloadScene (_LoadedMap);
			if (Mode == Multiplayer.Modes.Client) {
				NetManager.StopClient ();
			} else if (Mode == Multiplayer.Modes.Host) {
				NetManager.StopHost ();
			} else if (Mode == Multiplayer.Modes.Server) {
				NetManager.StopServer ();
			}
			ShowPlayMenu (true);
			_LoadedMap = "";
			//*/
		}
	}

	void Start ()
	{
		DontDestroyOnLoad (gameObject);
		DontDestroyOnLoad (EventSystem);
		_LoadedMap = "";
		_ShowPlayEnabled = true;
		_CameraStartGame = GetComponentInChildren<Camera> ();
		_MenuStartGame = GetComponent<Canvas> ();

		_SetIPInputFiled = SetIPInputFiled.GetComponent<InputField> ();
		_SetIPInputFiled.text = IP;

		_SetPortInputFiled = SetPortInputFiled.GetComponent<InputField> ();
		_SetPortInputFiled.text = PORT.ToString ();

		_ModeDropdown = ModeDropdown.GetComponent<Dropdown> ();
		List<string> Options = new List<string> (System.Enum.GetNames (typeof(Multiplayer.Modes)));
		_ModeDropdown.options.Clear ();
		foreach (string Option in Options) {
			_ModeDropdown.options.Add (new Dropdown.OptionData (Option));
		}
		_ModeDropdown.value = (int)Mode;
		ModeChanged ((int)Mode);

		_PlayButton = PlayButton.GetComponentInChildren<Text> ();
		_PlayButton.text = "Połącz";
	}

	void OnStopClient()
	{
		ShowPlayMenu (true);
		SceneManager.UnloadScene (_LoadedMap);
		_LoadedMap = "";
	}
	void OnStartClient(NetworkClient Client)
	{
		ShowPlayMenu (false);
	}
	void FixedUpdate()
	{
		if (Input.GetKeyDown (KeyCode.Escape) && _LoadedMap!="") {
			ShowPlayMenu (!_ShowPlayEnabled);
		}
	}
}
