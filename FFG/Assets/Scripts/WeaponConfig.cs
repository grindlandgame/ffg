﻿using UnityEngine;

public class WeaponConfig : MonoBehaviour
{
    public int Id;
    public string Name;
    public float DMG;
    public int Rate;
    public Transform BulletSpawn;
    public float BulletSpeed;
}
