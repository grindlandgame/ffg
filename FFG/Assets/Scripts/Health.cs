﻿using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {

    public float MaxHealth;
    public bool Respawnable;
    public string SpawnpointsTag;

    [SyncVar]public float Heal;

    private GameObject[] Spawnpoints;

    [Command]
    public void CmdUpdateHeal(float heal)
    {
        Heal = heal;
    }
    [Command]
    public void CmdTakeDamage(float DMG)
    {
        Heal -= DMG;
    }

    void Respawn()
    {
        if(isLocalPlayer)
        {
            Vector3 SpawnPos = Vector3.zero;
            if(Spawnpoints.Length>0)
            {
                SpawnPos = Spawnpoints[Random.Range(0, Spawnpoints.Length)].transform.position;
            }
            transform.position = SpawnPos;
            Heal = MaxHealth;
            CmdUpdateHeal(Heal);
        }
    }

	void Start () {
        Spawnpoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
        Heal = MaxHealth;
	}
	void FixedUpdate () {
        if(Heal <= 0f)
        {
            Respawn();
        }
	}
}
