﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class MedPackPickup : NetworkBehaviour
{

    public float AddHP;
    public bool ReUsable;
    public float RecoilTime;

    [SyncVar(hook = "OnVisibleChanged")]
    public bool visible;

    [ServerCallback]
    public void OnTriggerStay(Collider other)
    {
        StartCoroutine(handlePickup());

        GameObject Colided = other.gameObject;
        PlayerController Player = Colided.GetComponent<PlayerController>();
        if (Player != null)
        {
            Health Heal = Colided.GetComponent<Health>();
            Debug.Log("Use: " + Player.Use.ToString());
            if (Heal != null && Player.Use)
            {
                float _Heal = Mathf.Clamp(Heal.Heal + AddHP, 0, Heal.MaxHealth);
                Heal.CmdUpdateHeal(_Heal);
                if(!ReUsable)
                {
                    NetworkServer.Destroy(gameObject);
                }
            }
        }

    }

    public void OnVisibleChanged(bool newValue)
    {
        visible = newValue;
    }
    public override void OnStartServer()
    {
        visible = true;
    }

    public override void OnStartClient()
    {
        OnVisibleChanged(visible);
    }
    [Server]
    IEnumerator handlePickup()
    {
        visible = false;
        yield return new WaitForSeconds(RecoilTime);
        visible = true;
    }
}

