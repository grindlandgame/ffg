﻿using UnityEngine;
using UnityEngine.Networking;

public class PickupSpawner : NetworkBehaviour {

    public Object PickupToSpawn;
    private GameObject Spawned;
	void Start () {
        Spawned = Instantiate(PickupToSpawn,transform.position,transform.rotation) as GameObject;
        NetworkServer.Spawn(Spawned);
        Destroy(gameObject);
	}
}
