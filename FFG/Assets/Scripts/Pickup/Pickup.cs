﻿
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Pickup : NetworkBehaviour
{
    public bool ReUsable;
    public float RecoilTime;

    [SyncVar(hook = "OnVisibleChanged")]
    public bool visible;
    [ServerCallback]
    public void Effect(Collider other)
    {

    }

    [ServerCallback]
    public void OnTriggerStay(Collider other)
    {
        StartCoroutine(handlePickup());
        Effect(other);
    }

    public void OnVisibleChanged(bool newValue)
    {
        visible = newValue;
    }
    public override void OnStartServer()
    {
        visible = true;
    }

    public override void OnStartClient()
    {
        OnVisibleChanged(visible);
    }
    [Server]
    IEnumerator handlePickup()
    {
        visible = false;
        yield return new WaitForSeconds(RecoilTime);
        visible = true;
    }
}

