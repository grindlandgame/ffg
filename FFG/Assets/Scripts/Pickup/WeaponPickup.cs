﻿using UnityEngine;
using UnityEngine.Networking;

public class WeaponPickup : NetworkBehaviour
{
    public Object WeaponObject;
    public float RecoilTime;

    private WeaponConfig _WeaponConfig;
    private GameObject Spawned;

    [ServerCallback]
    public void OnTriggerStay(Collider other)
    {
        GameObject Colided = other.gameObject;
        PlayerController Player = Colided.GetComponent<PlayerController>();
        if (Player != null)
        {
            WeaponController Weapon = Colided.GetComponent<WeaponController>();
            if (Weapon != null && Player.Use)
            {
                int OldID = Weapon.WeaponID;
                Weapon.CmdChangeWeapon(_WeaponConfig.Id);
                WeaponObject = Weapon.WeaponPrefab[OldID];
                Refresh();
            }
        }
    }
    [ClientRpc]
    public void RpcRefresh()
    {
        Refresh();
    }
    [Command]
    public void CmdRefresh()
    {
        Refresh();
        RpcRefresh();
    }
    public void Refresh()
    {
        if(Spawned != null)
        {
            NetworkServer.Destroy(Spawned);
        }
        Spawned = Instantiate(WeaponObject, transform.position, transform.rotation, transform) as GameObject;
        NetworkServer.Spawn(Spawned);
        _WeaponConfig = Spawned.GetComponent<WeaponConfig>();
    }
    public override void OnStartServer()
    {
        Spawned = Instantiate(WeaponObject, transform.position,transform.rotation,transform) as GameObject;
        NetworkServer.Spawn(Spawned);
        _WeaponConfig = Spawned.GetComponent<WeaponConfig>();
    }
}

