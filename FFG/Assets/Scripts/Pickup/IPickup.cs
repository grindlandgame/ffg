﻿using UnityEngine;

interface IPickup
{
    void OnStartServer();
    void OnStartClient();
    void OnTriggerEnter(Collider other);
    void OnVisibleChanged(bool newValue);

}

