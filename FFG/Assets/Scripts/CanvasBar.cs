﻿using UnityEngine;

public class CanvasBar : MonoBehaviour {

    public RectTransform _BarPanel;
    public RectTransform _FiledImage;
    public int StartPrecentage;

    private float Width;
    private int FiledWidth;

    public void SetPrecentage(int P)
    {
        P = 100 - P;
        FiledWidth = (int)((P * Width) / 100);
        Vector2 OffsetMax = _FiledImage.offsetMax;
        OffsetMax.x = -FiledWidth;
        _FiledImage.offsetMax = OffsetMax;

    }
    public void Refresh()
    {
        Width = _BarPanel.rect.width;
    }
    void Start()
    {
        Refresh();
        SetPrecentage(StartPrecentage);
    }
}
