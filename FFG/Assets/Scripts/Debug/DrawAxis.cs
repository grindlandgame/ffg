﻿using UnityEngine;
using System.Collections;

public class DrawAxis : MonoBehaviour {

    public bool XRedRight;
    public bool YGreenUp;
    public bool ZBlueForward;
    public Color ColorX;
    public Color ColorY;
    public Color ColorZ;
    public float Length;
    private Transform _Transform;
	void Start () {
        _Transform = GetComponent<Transform>();
	}
	void Update () {

        if(XRedRight)
        {
            Debug.DrawLine(_Transform.position, _Transform.position + (_Transform.right * Length), ColorX);
        }
        if (YGreenUp)
        {
            Debug.DrawLine(_Transform.position, _Transform.position + (_Transform.up * Length), ColorY);
        }
        if (ZBlueForward)
        {
            Debug.DrawLine(_Transform.position, _Transform.position + (_Transform.forward * Length), ColorZ);
        }
    }
}
