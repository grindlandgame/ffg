﻿using UnityEngine;
using System.Collections;

public class TransformFollow : MonoBehaviour {

    public Transform Follow;
    public bool Position;
    public bool Rotation;
	void Update () {
        if(Position)
            transform.position = Follow.position;
        if(Rotation)
            transform.rotation = Follow.rotation;
	}
}
