﻿using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(PlayerController))]
public class CrosshairController : MonoBehaviour {

    public PlayerController _PlayerController;
    public WeaponController _WeaponController;
    public CameraController _CameraController;
    public RectTransform _RectTransform;
    public Image _Image;
    public GameObject _CanvasObject;


    void Start () {
	    if(!_PlayerController.isLocalPlayer)
        {
            Destroy(_CanvasObject);
            Destroy(this);
        }
	}
	
	void Update () {
        //*
        if (_WeaponController.Hit)
        {
            Vector3 Pos;
            if (_WeaponController.Hit)
            {
                Pos = _CameraController._Camera.WorldToScreenPoint(_WeaponController.WeaponHit.point);        
            }
            else
            {
                Pos = _CameraController._Camera.WorldToScreenPoint(_WeaponController._WeaponConfig.BulletSpawn.position + (_WeaponController._WeaponConfig.BulletSpawn.forward * 1000f));
            }
            _RectTransform.position = Pos;
        }//*/

    }
}
