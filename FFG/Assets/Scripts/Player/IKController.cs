﻿using UnityEngine;

public class IKController : MonoBehaviour
{

    public CharacterController _CharacterController;
    public float FallingSpeed;
    void OnAnimatorIK()
    {
        if (!_CharacterController.isGrounded)
        {
            _CharacterController.SimpleMove(Vector3.down * FallingSpeed);
        }
        
    }
}