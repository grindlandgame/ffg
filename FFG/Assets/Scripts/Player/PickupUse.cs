﻿
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class PickupUse : MonoBehaviour {

    public PlayerController _PlayerController;
    void OnTriggerStay(Collider colide)
    {
        GameObject Colided = colide.gameObject;
        if(Colided.CompareTag("Pickup"))
        {
            if(_PlayerController.Use)
            {
                Colided.SendMessage("CmdUse", gameObject);
            }
        }
    }
}
