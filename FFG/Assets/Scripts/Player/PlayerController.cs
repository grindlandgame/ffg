﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(WeaponController))]
[RequireComponent(typeof(CameraController))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(IKController))]

public class PlayerController : NetworkBehaviour
{
    public CameraController _CameraController;
    public WeaponController _WeaponController;
    public Animator _Animator;
    public CharacterController _CharacterController;
    public float RotateSensity;

    [HideInInspector]
    public float MouseX; //Mouse movement X
    [HideInInspector]
    public float MouseY; //Mouse movement Y
    private float Vertical; //W S
    private float Horizontal; //A D
    private bool Crouching; //
    //Animator
    //private bool Jump; //Key //Animator
    private bool NextWeapon; //Key
    [HideInInspector]
    public bool FireMain; //Key
    private bool Sprint; //Key
    [HideInInspector]
    public bool Use;//Key

    private float Forward; //Animator
    private float Turn; //Animator
    private bool OnGround;  //Animator
    [HideInInspector]
    public float AimHeight; //Animator
    //private float JumpState; //Animator
    [Command]
    private void CmdSendKeys(bool _Use)
    {
        Debug.Log("Server reciving!");
        Use = _Use;
    }
    private void GetInputFromPlayer()//For only Local player
    {
        MouseX = Input.GetAxis("Mouse X");
        MouseY = Input.GetAxis("Mouse Y");
        Vertical = Input.GetAxis("Vertical");
        Horizontal = Input.GetAxis("Horizontal");

        Crouching = Input.GetButton("Crouching");
        //NextWeapon = Input.GetButtonDown("Next weapon");
        NextWeapon = false;
        FireMain = Input.GetButton("FireMain");
        Sprint = Input.GetButton("Sprint");
        if(Use != Input.GetButton("Use"))
        {
            Debug.Log("Client Sending!");
            CmdSendKeys(Input.GetButton("Use"));
        }
        Use = Input.GetButton("Use");
        /*
        if(Use != Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Client Sending!");
            CmdSendKeys(Input.GetKeyDown(KeyCode.E));
        }
        Use = Input.GetKeyDown(KeyCode.E);///*/
        Debug.Log("Client Use: " + Use.ToString());
        //Jump = Input.GetButton("Jump");
    }

    private void StartLocalPlayer()
    {
        gameObject.tag = "LocalPlayer"; 
        _Animator.applyRootMotion = true;
        _CameraController._Camera.enabled = true;
    }
    private void UpdateLocalPlayer()
    {
        GetInputFromPlayer();

        if(NextWeapon)
        {
            _WeaponController.CmdChangeWeapon(_WeaponController.WeaponID + 1);
        }
        /*//For Jump Animation
        if(JumpState>0f)
        {
            Jump = true;
            OnGround = false;
            JumpState += 1f * Time.deltaTime;
            if (JumpState > 1f)
            {
                JumpState = 0f;
                Jump = false;
                OnGround = true;
            }
        }
        else if(Jump)
        {
            JumpState += 1f * Time.deltaTime;
            OnGround = false;
        }//*/


        Vector3 PlayerAngle = transform.eulerAngles;
        PlayerAngle.y += MouseX * Time.deltaTime * RotateSensity;
        transform.eulerAngles = PlayerAngle;//*/



        if (!Sprint)
        {
            Forward = Vertical / 2;
            Turn = Horizontal / 2;
        }

        OnGround = _CharacterController.isGrounded;

        _Animator.SetFloat("Forward", Forward);
        _Animator.SetFloat("Turn", Turn);
        _Animator.SetBool("OnGround", OnGround);
        _Animator.SetBool("Crouching", Crouching);
        _Animator.SetFloat("AimHeight", AimHeight);

        //_Animator.SetBool("Jump", Jump);
        //_Animator.SetFloat("JumpState", JumpState);
        //TODO: Add a Jump Animation
    }
    void Start()
    {
        if(isLocalPlayer)
        {
            StartLocalPlayer();
        }
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            UpdateLocalPlayer();
        }
    }
}