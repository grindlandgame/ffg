﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerController))]
public class WeaponController : NetworkBehaviour {

    public PlayerController _PlayerController;
    public Object Bullet;
    public Object[] WeaponPrefab;
    public Transform _ParentTransform;
    public Transform _LeftHandTransform;
    public Transform _RightHandTransform;
    public Transform _WeaponPosCalculator;

    [HideInInspector]
    public int WeaponID;
    [HideInInspector]
    public GameObject _WeaponObject;
    [HideInInspector]
    public WeaponConfig _WeaponConfig;
    [HideInInspector]
    public RaycastHit WeaponHit;
    [HideInInspector]
    public bool Hit;

    private float TimeToNextBullet;
    private float TimeLastBullet;
    private Vector3 WeaponPosition;


    [Command]
    public void CmdWeaponRefreshID()
    {
        ChangeWeapon(WeaponID);
        RpcChangeWeapon(WeaponID);
    }
    [Command]
    public void CmdChangeWeapon(int id)
    {
        WeaponID = id;
        ChangeWeapon(WeaponID);
        RpcChangeWeapon(WeaponID);
    }
    [ClientRpc]
    public void RpcChangeWeapon(int id)
    {
        ChangeWeapon(id);
    }
    [Command]
    public void CmdFire()
    {
        if(Hit)
        {
            GameObject Hited = WeaponHit.collider.gameObject;
            Health _Health = Hited.GetComponent<Health>();
            if(_Health != null)
            {
                _Health.CmdTakeDamage(_WeaponConfig.DMG);
            }

        }
    }
    public void Fire()
    {
        if (TimeLastBullet > TimeToNextBullet)
        {
            TimeLastBullet = 0f;
            if (!isLocalPlayer)
                return;
            Quaternion rotation = _WeaponConfig.BulletSpawn.rotation;
            Vector3 position = _WeaponConfig.BulletSpawn.position;
            GameObject SpawnedBullet = Instantiate(Bullet, position, rotation) as GameObject;
            Vector3 forceBullet = SpawnedBullet.transform.forward;
            Rigidbody rigidbodyBullet = SpawnedBullet.GetComponent<Rigidbody>();
            rigidbodyBullet.AddForce(forceBullet * _WeaponConfig.BulletSpeed, ForceMode.Impulse);
            Destroy(SpawnedBullet, 5f);
            if (Hit)
            {
                CmdFire();
            }
        }
        else
        {
            TimeLastBullet += Time.deltaTime;
        }
    }
    public void ChangeWeapon(int id)
    {
        WeaponID = Mathf.Abs(id) % WeaponPrefab.Length;
        if(_WeaponObject != null)
        {
            Destroy(_WeaponObject);
        }
        _WeaponObject = Instantiate(WeaponPrefab[WeaponID],WeaponPosition,Quaternion.identity,_ParentTransform) as GameObject;
        _WeaponConfig = _WeaponObject.GetComponent<WeaponConfig>();
        TimeToNextBullet = 1f / _WeaponConfig.Rate;
    }
    void StartLocalPlayer()
    {
        TimeLastBullet = 1f;
        CmdChangeWeapon(WeaponID);
    }
    void UpdateLocalPlayer()
    {
        if(_PlayerController.FireMain)
        {
            Fire();
        }
    }
    void Start ()
    {
        if(isLocalPlayer)
        {
            StartLocalPlayer();
        }
        else
        {
            ChangeWeapon(WeaponID);
            CmdWeaponRefreshID();
        }
    }

	void Update ()
    {
        if (isLocalPlayer)
        {
            UpdateLocalPlayer();
        }
        WeaponPosition = (_LeftHandTransform.position + _RightHandTransform.position) / 2f;
        _WeaponObject.transform.position = WeaponPosition;
        _WeaponObject.transform.LookAt(_LeftHandTransform);
        _WeaponPosCalculator.position = WeaponPosition;

        Hit = Physics.Raycast(_WeaponConfig.BulletSpawn.position, _WeaponConfig.BulletSpawn.forward, out WeaponHit, Mathf.Infinity);

    }
}
