﻿using UnityEngine;

[RequireComponent(typeof(Health))]
public class UIController : MonoBehaviour {

    public PlayerController _PlayerController;
    public CanvasBar _HealthBar;
    public Health _Health;
    
    void Start()
    {
        if(!_PlayerController.isLocalPlayer)
        {
            Destroy(_HealthBar.gameObject);
            Destroy(this);
        }
    }

    void FixedUpdate()
    {
        int Precentage = (int)( (_Health.Heal * 100)/_Health.MaxHealth );
        _HealthBar.SetPrecentage(Precentage);
    }
}
