﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WeaponController))]
public class CameraController : NetworkBehaviour {

    public PlayerController _PlayerController;
    public WeaponController _WeaponController;
    public Camera _Camera;
    public Transform _CameraTransform;
    public float MinX;
    public float MaxX;
    public float CameraSensityX;
    public float CameraSensityY;

    [HideInInspector]
    [SyncVar]public Vector3 _WeaponPosCalculatorRotation;
    [HideInInspector]
    public RaycastHit RayCamera;

    [Command]
    public void CmdUpdatePosCalculator(Vector3 Rotation)
    {
        _WeaponPosCalculatorRotation = Rotation;
    }

    private Vector3[] LastPos;
    private int LastPosIterator;
    private void UpdateLocalPlayer()
    {
        Vector3 Pos = _WeaponController._WeaponObject.transform.position;
        LastPos[LastPosIterator] = Pos;
        LastPosIterator++;
        LastPosIterator %= 10;
        Pos = Vector3.zero;
        for (int i = 0; i < 10; i++)
        {
            Pos += LastPos[i];
        }
        Pos /= 10f;
        Pos += -transform.forward * 1.5f;
        Pos += transform.right * 1.5f;
        Pos += transform.up * 0f;
        _CameraTransform.position = Pos;

        _PlayerController.AimHeight += Time.deltaTime * _PlayerController.MouseY * CameraSensityY/90;
        _PlayerController.AimHeight = Mathf.Clamp(_PlayerController.AimHeight, -0.999f, 0.999f);

        Pos.y += -_PlayerController.AimHeight * 2f;
        _CameraTransform.position = Pos;
        

        Physics.Raycast(_CameraTransform.position, _CameraTransform.forward, out RayCamera, Mathf.Infinity);
        _WeaponController._WeaponPosCalculator.LookAt(RayCamera.point);
        _WeaponPosCalculatorRotation = _WeaponController._WeaponPosCalculator.eulerAngles;
        CmdUpdatePosCalculator(_WeaponPosCalculatorRotation);


        Vector3 Rotation = _WeaponController._WeaponObject.transform.eulerAngles;
        _CameraTransform.eulerAngles = Rotation;
        CmdUpdatePosCalculator(_WeaponPosCalculatorRotation);

    }
    private void UpdateNonLocalPlayer()
    {
        if(!isLocalPlayer)
        {
            _WeaponController._WeaponPosCalculator.eulerAngles = _WeaponPosCalculatorRotation;
        }
    }
    private void StartLocalPlayer()
    {
        LastPos = new Vector3[10];
        for (int i = 0; i < 10; i++)
        {
            LastPos[i] = _WeaponController._WeaponObject.transform.position;
        }
        LastPosIterator = 0;
        _CameraTransform.parent = null as Transform;
    }
    void Start()
    {
        if(_PlayerController.isLocalPlayer)
        {
            StartLocalPlayer();
        }
    }
    void Update ()
    {
        if (_PlayerController.isLocalPlayer)
        {
            UpdateLocalPlayer();
        }
        else
        {
            UpdateNonLocalPlayer();
        }
    }
}
